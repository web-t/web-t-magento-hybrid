<?php
/**
 * Register europeancommission_webt module
 *
 * @package Webt
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'europeancommission_webt',
    __DIR__
);
