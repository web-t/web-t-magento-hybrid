<?php

namespace europeancommission\webt\Block\Adminhtml;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Config extends Template
{

    /**
     * Config
     *
     * @var Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Initialize Config class
     *
     * @param Context              $context Template context.
     * @param ScopeConfigInterface $scopeConfig Config for this scope.
     */
    public function __construct(Context $context, ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get Translation Hub URL
     *
     * @return string
     */
    public function getTranslationHubUrl()
    {
        return $this->scopeConfig->getValue('webt_general/webt_th_connection/url');
    }

    /**
     * Get Translation Hub Client ID
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->scopeConfig->getValue('webt_general/webt_th_connection/id');
    }

    /**
     * Get excluded paths
     *
     * @return array
     */
    private function getExcludedPaths()
    {
        $paths = $this->scopeConfig->getValue('webt_general/webt_settings/exclude_certain_paths');
        if ($paths) {
            return array_filter(preg_split('/\r\n|\r|\n/', trim($paths)));
        }
        return [];
    }

    /**
     * Load saved config as JSON string
     *
     * @return string
     */
    public function getJsConfig()
    {
        $config = [
            'translationHubUrl' => $this->getTranslationHubUrl(),
            'clientId'          => $this->getClientId(),
            'showMtNotice'      => $this->scopeConfig->getValue('webt_general/webt_settings/show_mt_notice') !== 'no',
            'excludePaths'      => $this->getExcludedPaths(),
        ];

        return json_encode($config);
    }
}
