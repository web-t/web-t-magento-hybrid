<?php
namespace europeancommission\webt\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Custom YesNo where Yes is default value
 */
class YesNo implements OptionSourceInterface
{

    /**
     * Retrieve options as array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'yes',
                'label' => __('Yes'),
            ],
            [
                'value' => 'no',
                'label' => __('No'),
            ],
        ];
    }
}
