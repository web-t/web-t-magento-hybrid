<?php

namespace europeancommission\webt\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Message\ManagerInterface as MessageManager;

/**
 * Observer for WEB-T settings save event
 */
class SaveSettingsObserver implements ObserverInterface
{

    /**
     * The Cache Manager object for managing caches.
     *
     * @var \Magento\Framework\App\Cache\Manager
     */
    protected $cacheManager;

    /**
     * The Message Manager object for displaying messages.
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * The ScopeConfigInterface object for accessing configuration settings.
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $config;

    /**
     * The Curl object for making HTTP requests.
     *
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;

    /**
     * SaveSettingsObserver constructor.
     *
     * @param \Magento\Framework\App\Cache\Manager               $cacheManager The Cache Manager object for managing caches.
     * @param \Magento\Framework\Message\ManagerInterface        $messageManager The Message Manager object for displaying messages.
     * @param \Magento\Framework\HTTP\Client\Curl                $curl The Curl object for making HTTP requests.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config The ScopeConfigInterface object for accessing configuration settings.
     */
    public function __construct(
        \Magento\Framework\App\Cache\Manager $cacheManager,
        MessageManager $messageManager,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
    ) {
        $this->cacheManager   = $cacheManager;
        $this->messageManager = $messageManager;
        $this->config         = $config;
        $this->curl           = $curl;
    }

    /**
     * Checks connection with Translation hub after settings are saved
     *
     * @param Observer $observer Observer object.
     * @return void
     */
    public function execute(Observer $observer)
    {
        // Check connection with Translation Hub.
        $th_url = $this->config->getValue('webt_general/webt_th_connection/url');
        $id     = $this->config->getValue('webt_general/webt_th_connection/id');
        $url    = "$th_url/api/configurationservice/configuration/$id";

        $this->curl->get($url);
        $statusCode = $this->curl->getStatus();

        if (200 !== $statusCode) {
            $this->messageManager->addErrorMessage('There was an error connecting to Translation Hub. Please try again or recheck your configuration.');
        } else {
            $this->messageManager->addSuccessMessage('Connection with Translation Hub successful.');
        }

        // Clean cache.
        $this->cacheManager->clean([ 'compiled_config', 'full_page' ]);
    }
}
